﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class AboutPage : ContentPage
    {

        private double n1, n2, r;
        string o;

        public AboutPage()
        {
            InitializeComponent();
            Title = "Calculator";
        }

        public void Handle_Appearing(object sender, System.EventArgs e)
        {
            DisplayAlert("Hi", "Welcome to the calculator", "Start");

        }

        public void Calculator(object sender, EventArgs e)
        {
            // Check for errors in nb1
            bool isNumeric = double.TryParse(nb1.Text, out _);

            if (isNumeric)
            {
                n1 = double.Parse(nb1.Text);
            }
            else
            {
                DisplayAlert("Error :(", "Choose a correct number", "go");
                nb1.Text = "";
                nb2.Text = "";
                opt.Text = "";
                return;
            }

            // Check for errors in nb2

            isNumeric = double.TryParse(nb2.Text, out _);

            if (isNumeric)
            {
                n2 = double.Parse(nb2.Text);

            }
            else
            {
                DisplayAlert("Error :(", "Choose a correct number", "go");
                nb1.Text = "";
                nb2.Text = "";
                opt.Text = "";
                return;
            }

            // Check for errors in the operator

            if (opt.Text.Contains("+") || opt.Text.Contains("-") ||
                opt.Text.Contains("/") || opt.Text.Contains("*"))
            {
                if (opt.Text.Length != 1)
                {
                    DisplayAlert("Error :(", "Choose one operator", "go");
                    nb1.Text = "";
                    nb2.Text = "";
                    opt.Text = "";
                    return;
                }
                else
                    o = opt.Text;
            }
            else
            {
                DisplayAlert("Error :(", "Choose a correct operator", "go");
                nb1.Text = "";
                nb2.Text = "";
                opt.Text = "";
                return;
            }

            switch (o)
            {
                case "+":
                    r = n1 + n2;
                    break;
                case "x":
                    r = n1 * n2;
                    break;
                case "-":
                    r = n1 - n2;
                    break;
                case "/":
                    r = n1 / n2;
                    break;
                default:
                    break;
            }

            DisplayAlert("Hi", "The result is " + r.ToString(), "Try another one");

            n1 = 0;
            n2 = 0;
            o = "";
            r = 0;

        }
    }
}