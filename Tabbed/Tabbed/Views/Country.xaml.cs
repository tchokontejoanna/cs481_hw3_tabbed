﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Tabbed.Views
{
    public partial class Country : ContentPage
    {
        int i = 4;

        public Country()
        {
            InitializeComponent();

            Title = "Guess the country";
            var image = new Image { Source = "france.png" };
            image.Source = Device.RuntimePlatform == Device.Android
                            ? ImageSource.FromFile("france.png")
                            : ImageSource.FromFile("Images/france.png");
        }

        public void Handle_Appearing(object sender, System.EventArgs e)
        {
            DisplayAlert("Hello", "You have 3 chances to find the country", "Got it ");
        }

        public void Handle_Disappearing(object sender, System.EventArgs e)
        {
            DisplayAlert("Goodbye !", "Hope you know more about this animal", "see you next time");
        }

        public void Validate(object sender, System.EventArgs e)
        {
            if (i == 0)
            {
                DisplayAlert("Sorry", "You can't try anymore, the answer is France", "Got it ");

            }
            if (country.Text == "France")
            {
                DisplayAlert("Well done !", "It's the flag of France", ":)");
            }
            else
            {
                i--;
                DisplayAlert("Sorry", "You get to try " + i + " more times", "Try again");
                country.Text = "";
            }

        }
    }
}
